These modifications allow [Exportify](https://rawgit.com/watsonbox/exportify) to export to an iTunes-compatible playlist format. This will allow you to import your Spotify playlists for use with, for example, the new Apple Music streaming service. There are caveats, however:

### Apple Music doesn't automatically add new tracks
The biggest problem is that iTunes doesn't support importing playlists that contain tracks that aren't already in your My Music library. You have to manually go into Apple Music, search for the album or track, and hit the "+" to add it to My Music. Only then will iTunes allow you to import the playlist. Hopefully Apple updates iTunes to support automatically adding tracks from imported playlists in the future.

### Track naming disparity
Unfortunately there is some large disparity between Spotify's and iTunes Store's track naming schemes. Remixes, for example, are often denoted by a hyphen in Spotify, but are wrapped in brackets on iTunes. iTunes does a fairly good job of guessing at matches in these cases, but it's simply not 100%. Spotify also properly attributes tracks with multiple artists involved, but iTunes only seems to commonly attribute the album artist.

### Installation and setup
Please review the [README from the original repo.](https://rawgit.com/watsonbox/exportify)
